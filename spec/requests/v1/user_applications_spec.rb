require 'swagger_helper'

RSpec.describe 'v1/user_applications', type: :request do

  path '/v1/jobs/{job_id}/user_applications' do
    # You'll want to customize the parameter types...
    parameter name: 'job_id', in: :path, type: :string, description: 'job_id'

    post('create user_application') do
      response(200, 'successful') do
        let(:job_id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/v1/jobs/{job_id}/user_applications/new' do
    # You'll want to customize the parameter types...
    parameter name: 'job_id', in: :path, type: :string, description: 'job_id'

    get('new user_application') do
      response(200, 'successful') do
        let(:job_id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/v1/jobs/{job_id}/user_applications/{id}' do
    # You'll want to customize the parameter types...
    parameter name: 'job_id', in: :path, type: :string, description: 'job_id'
    parameter name: 'id', in: :path, type: :string, description: 'id'

    get('show user_application') do
      response(200, 'successful') do
        let(:job_id) { '123' }
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    delete('delete user_application') do
      response(200, 'successful') do
        let(:job_id) { '123' }
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/v1/user_posts/{user_post_id}/user_applications' do
    # You'll want to customize the parameter types...
    parameter name: 'user_post_id', in: :path, type: :string, description: 'user_post_id'

    get('list user_applications') do
      response(200, 'successful') do
        let(:user_post_id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/v1/user_posts/{user_post_id}/user_applications/{id}' do
    # You'll want to customize the parameter types...
    parameter name: 'user_post_id', in: :path, type: :string, description: 'user_post_id'
    parameter name: 'id', in: :path, type: :string, description: 'id'

    get('show user_application') do
      response(200, 'successful') do
        let(:user_post_id) { '123' }
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end
end
