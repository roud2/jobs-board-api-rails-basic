require 'rails_helper'

describe JobApplication, type: :model do

    context "Job application" do

        it "Job required" do
            app_user = User.create(email: "app_user@email.com", password: "testtest")
            application = JobApplication.create(user_id: app_user.id, job_id: nil)
            expect(application).to_not be_valid
        end    
        it "User required" do
            owner_user = User.create(email: "owner@email.com", password: "testtest")
            job = Job.create(user_id: owner_user.id,job_title:"job title", job_description: "Job description")
            application = JobApplication.create(user_id: nil, job_id: job.id)
            expect(application).to_not be_valid
        end

    end
    context "Validation" do
        it "'Not Seen'" do
            app_user = User.create(email: "app_user@email.com", password: "testtest")
            owner_user = User.create(email: "owner_user@email.com", password: "testtest")
            job = Job.create(user_id: owner_user.id,job_title:"job title", job_description: "Job description")
            application = JobApplication.create(user_id: app_user.id, job_id: job.id)
            expect(application.status).to eq('Not Seen')
        end
        it "valid" do
            app_user = User.create(email: "app_user@email.com", password: "testtest")
            owner_user = User.create(email: "owner_user@email.com", password: "testtest")
            job = Job.create(user_id: owner_user.id,job_title:"job title", job_description: "Job description")
            application = JobApplication.create(user_id: app_user.id, job_id: job.id,status: "seen")
            expect(application).to be_valid
        end
    end
end