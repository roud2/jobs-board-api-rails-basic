require 'rails_helper'
RSpec.describe User, type: :model do
   context "Data" do
      it 'Email required' do 
         user = User.new(email: nil, password: "testtest")
         expect(user).to_not be_valid
      end
      it 'Password required' do
         user = User.new(email: "owner@email.com", password: nil)
         expect(user).to_not be_valid
      end
   end
   context "Validation test" do
      it "valid" do
         user = User.new(email: "owner@email.com",password: "testtest")
         expect(user).to be_valid
      end
   end
end