require 'rails_helper'

RSpec.describe Job, type: :model do
    context "Constrains test" do
        it "Needs user" do
            post = Job.new(user_id: nil, job_title: "job title", job_description: "Job description")
            expect(post).to_not be_valid
        end
        it "Title required" do
            owner = User.new(email: "owner@email.com", password: "testtest123")
            post = Job.new(user_id: owner.id, job_title: nil, job_description: "Job description")
            expect(post).to_not be_valid
        end
        it "Description required" do
            owner = User.new(email: "owner@email.com", password: "testtest123")
            post = Job.new(user_id: owner.id, job_title: "job title", job_description: nil)
            expect(post).to_not be_valid
        end
    end
    context "Validation test" do
        it "Valid" do
            owner = User.new(email: "owner@email.com", password: "testtest123")
            post = Job.new(user_id: owner.id, job_title: "job title", job_description: "Discription") 
            expect(post).to_not be_valid
        end
    end
end