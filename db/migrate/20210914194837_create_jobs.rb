class CreateJobs < ActiveRecord::Migration[6.1]
  def change
    create_table :jobs do |t|
      t.belongs_to :user, foreign_key: true
      t.string :job_title,limit:255,null: false
      t.string :job_description, limit: 1000, null: false
      t.date :job_dead_line
      t.timestamps
    end
  end
end
