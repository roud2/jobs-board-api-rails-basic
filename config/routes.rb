Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  mount_devise_token_auth_for 'User', at: 'v1', defaults: {format: :json}, controllers: {
    registrations:'token_auth_override/registrations', #CUSTOM

  }
  as :user do
    # Define routes for User within this block.
    namespace :v1  do
      resources :jobs, only: [:index, :show, :create]do
        resources :user_applications, only: [:new, :create, :show, :destroy,]
      end
      resources :user_posts, only: [:new, :index, :create, :show, :update, :destroy] do
        resources :user_applications, only: [:index, :show]
      end   
      resources :users, only:[:create]   
    end
  end

end
