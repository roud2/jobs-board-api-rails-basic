class JobApplication < ApplicationRecord
    belongs_to :user
    belongs_to :job
    validates :user_id, uniqueness: {scope: :job_id}
    validates :status, acceptance: { accept: ['not seen', 'seen'] }
end
