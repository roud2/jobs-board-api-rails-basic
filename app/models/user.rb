class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,:recoverable, :rememberable, :validatable
  include DeviseTokenAuth::Concerns::User

  has_many :job_applications, dependent: :destroy
  has_many :jobs, dependent: :destroy
  
  accepts_nested_attributes_for :jobs
  accepts_nested_attributes_for :job_applications

end
