class Job < ApplicationRecord
    belongs_to :user
    validates :job_title, presence: true
    validates :job_description, presence: true
    has_many :job_applications, dependent: :destroy
    validates :user_id,presence: true
    accepts_nested_attributes_for :job_applications
end
