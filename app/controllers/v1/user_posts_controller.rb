class V1::UserPostsController < ApplicationController
    before_action :authenticate_user!
    before_action :set_user
    
    def new
        @job = Job.new
        render json: {data:@job.as_json(only:[:job_title,:job_description,:job_dead_line])}
    end

    def index
        @jobs = @user.jobs
        if @job.present?
            render json: {data: @jobs}, status: :ok
        else
            render json: {message: "User has no job posts"}
        end
    end

    def show
        @jobs = @user.jobs

        if @jobs.present?
            @job = @jobs.find_by(id: params[:id])

            if @job.present?
                render json:{data: @job}, status: :ok
            else
                render json:{message: "Job unavailable"}
            end
        else
            render json:{message: "Job unavailable"}
        end
    end

    def create
        @job = @user.jobs.new(job_params)

        if @job.save
            render json:{message: "Successful creation"}
        else
            render json: {message: "Failure: "+@job.errors.full_messages.join(", ")}

        end
    end

    def update 
        @jobs = @user.jobs
        
        if @jobs.present? 
            @job = @jobs.find_by(id: params[:id])

            if @job.present?
                if @job.update(job_params)
                    render json:{message: "Successful update"}
                else
                    render json:{message: "Failure: "+@job.errors.full_messages.join(", ")}
                end
            else
                render json: {message: "Job unavailable"}
            end
        else
            render json: {message: "Job unavailable"}
        end

    end

    def destroy
        @jobs = user.jobs

        if @jobs.present?
        @job =  @jobs.find_by(id:params[:id])
            if job.present?
                if @job.destroy
                    render json:{message: "Job deleted"}, status: :ok
                else
                    render json: {message: "Job unavailable"}
                end
            else
                render json: {message: "Job unavailable"}
            end
        end
    end

    private 
    def set_user
        @user =current_user
    end
    def job_params
        params.require(:post).permit(:job_title,:job_description)
    end

end