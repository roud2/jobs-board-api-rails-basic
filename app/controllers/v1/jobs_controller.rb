class V1::JobsController < ApplicationController
    before_action :authenticate_user!
    # before_action :set_user

    def index
        @jobs = Job.all
        if(@jobs.present?)
            render json: @jobs, status: :ok
        else
            render json:{message: "No job posts"}
        end
    end
    def show
        @job = Job.find_by(id: params[:id])
        
        if @job.present?
            render json: @job, status: :ok
        else 
            render json:{message: "not found"}
        end
    end
    private
    def job_params
        params.require(:job).permit(:id,:job_title,:job_description,:job_dead_line)
    end

end
