class V1::UserApplicationController < ApplicationController
    
    before_action :authenticate_user!
    before_action :set_user
    
    def new
        @application = JobApplication.new
        render json: {data: @application}
    end

    def show
        @application = @user.job_applications.find_by(id: params[:id])

        if @application.present?
            render json: {data: @application}, status: :ok
        else
            render json: {message: "Application unavailable"}
        end
    end

    def create
        @job = Job.find_by(params[:job_id])

        if @job.present?
            @application = @job.job_applications.create(user_id: @user.id)

            if @application.save
                render json: {message: "Application created"}, status: :ok
            else
                render json: {message: @application.errors.full_messages.join(', ')}
            end
        else
            render json: {message: "Job unavailable"}
        end
    end
    
    def destroy
        @job = Job.find_by(params[:job_id])

        if @job.present?
            @application = @job.job_applications.find_by(user_id: @user.id)

            if @application.present?
                @application.destroy
                render json:{message: "Application deleted"}, status: :ok
            else
                render json: {message: "Application unavailable"}
            end
        else
            render json: {message: "Job unavailable"}
        end
    end 
    
    private

    def set_user
        @user = current_user
    end


end
