class V1::JobApplicationsController < ApplicationController
    before_action :authenticate_user!
    before_action :set_user

    def index
        @job = Job.find_by(user_id: @user.id, id: params[:user_posts])

        if @job.present?            
            @application = @job.job_applications

            if @application.present?
                render json: {data: @application}, status: :ok
            else
                render json: {message: "No job application"}
            end
        else
            render json: {message: "Application is unavaliable"}
        end
    end

    def show
        @job = Job.find_by(user_id: @user.id, id: params[:user_posts] ).job_applications

        if @job.present?
            @application = @job.find_by(id: params[:id])

            if @application.present?
                @application.status = 'Seen'
                @application.save
                render json: {data: @application}, status: :ok
            else
                render json: {message: "Application is unavaliable"}
            end
        else
            render json: {message: "job is unavaliable"}
        end
    end

    private

    def set_user
        @user = current_user
    end

end
